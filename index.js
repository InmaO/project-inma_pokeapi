const fetchList = async () => {
    //Dirección de la llamada-API
    const ENDPOINT = "https://pokeapi.co/api/v2/pokemon?limit=10&offset=251";
    //Cogemos la base url de la imagen del detalle hasta la ruta donde está sprites/others/official-artwork/front_default
    const IMG_BASE_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/";
    const IMAGE_TYPE = ".png"
    const getIdFromURL = url => url.split('/').slice(0, -1).pop()
    const renderList = (lista, parent) => {
        //Relacionamos con el padre máximo (pokeNameContainer) con el child (div1)
        parent.innerHTML = "";
        //Recorremos el array para no hacerlo uno a uno, ya que tiene muchos datos. Con la lista separamos la carga de datos del render de datos
        lista.forEach(({name, url}) => {
            console.log("Dentro del bucle mi elemento" , name);
            //Vamos a poner los nombres dentro de una p que está en un div  
            //Vamos a crear el div
            const id = getIdFromURL(url)
            const div = document.createElement('div');
            div.id= id; 
            div.classList.add('card');
            //Creamos la p que estará dentro del padre(div) que está en la línea 30
            const pokeName = document.createElement('p');
            const pokeImage = document.createElement('img');
            pokeName.textContent = name;
            pokeImage.src = `${IMG_BASE_URL}${id}${IMAGE_TYPE}` 
            //Relacionamos con el padre (div1) con el child (pokeName)
            div.appendChild(pokeName);
            div.appendChild(pokeImage);
            parent.appendChild(div);
        });
    }
    //Esperando la respuesta de la API - Y si hay error imprimirlo por pantalla
    const pokemonListRequest = await fetch(ENDPOINT).catch((error) => console.log(error))
    console.log("Llamando a la API" , pokemonListRequest)
    //Si el server no me da 200, mi promesa ha sido rechazada/no tengo datos
    if(pokemonListRequest.status !==200){ 
        return Promise.reject;
    }
   //Transformar los datos enviados a datos que legibles
   const pokemonAnswer = await pokemonListRequest.json();
   console.log("Transformados mis datos por json" , pokemonAnswer)
    //Guardamos solo la info que nos interesa en una array y esto lo sacamos de pokemonAnswer, que tiene mucha info
    const { results } = pokemonAnswer;
    //Si preguntamos por cualquier cosa de la lista de pokemon, nos fallará porque no está en la lista de datos de json
    console.log("Datos que nos interesan" , results)

    //Buscar el padre máximo del html (el que está más arriba) para luego meter/poner los hijos div (que contienen un p con el nombre del pokemon)
    //Se pueden buscar a los elementos/etiquetas en html con el id mediante document.getElementById("mi id nombrado en html")
    const getParent = document.getElementById("pokeNameContainer")

    renderList(results, getParent)
    // Cogemos el input que hemos creado y le añadimos un listener al evento change. 
    // const searchInput = document.getElementById("searchTerm")
    // searchInput.addEventListener("change", e => console.log(e.target.value))
    // Cuando picas el botón te da lo que hay en el input y filtra la lista con lo que hay
    document.getElementById("searchButton").addEventListener("click", () => {
        const {value} = document.getElementById("searchTerm")
        console.log(value)
        const listaFiltrada = results.filter(({name}) => name.includes(value))
        console.log(listaFiltrada)
        renderList(listaFiltrada, getParent)
    })
}

fetchList();

